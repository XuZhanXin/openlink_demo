/* eslint-disable @typescript-eslint/no-unused-vars */
import { ospWallet } from "./account";
import address from "./address.json";
async function main() {
  const hash = await ospWallet.sendUserOperation({
    to: await ospWallet.getAddress(),
    value: 0,
    // @ts-ignore
    data: ospWallet.account.accountInterface.encodeFunctionData("setPermitCall", [address.openLink, address.mockTrade, true])
  });
  console.log(hash);
  console.log(await ospWallet.getUserOperationReceipt(hash));
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
