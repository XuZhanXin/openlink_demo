/* eslint-disable @typescript-eslint/no-unused-vars */
import fs from 'fs';
import { ethers } from "hardhat";
import { MockTrade__factory, TradeOpenlink__factory } from "../typechain-types";
import address from "./address.json";

async function main() {
  const mockTrade = await new MockTrade__factory(ethers.provider.getSigner()).deploy();
  address.mockTrade = mockTrade.address;
  const openLink = await new TradeOpenlink__factory(ethers.provider.getSigner()).deploy(address.osp, address.mockTrade);
  address.openLink = openLink.address;
  fs.writeFileSync('scripts/address.json', JSON.stringify(address, null, 2), 'utf-8');
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
