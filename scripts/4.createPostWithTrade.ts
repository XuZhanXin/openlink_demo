/* eslint-disable @typescript-eslint/no-unused-vars */

import { OspClient__factory, OspEvents__factory } from "osp-blockchain-types";
import { ospWallet } from "./account";
import address from './address.json';
import { ethers } from "hardhat";
import { BytesLike } from "ethers";
import { TradeOpenlink__factory } from "../typechain-types";

const communityId='0x5bc1a7109f07327345b68556ad80a1a1f83045a1edacfd7b7b4710f36d63904d';
const profileId=16;
async function main() {
  const ospInterface=OspClient__factory.createInterface();
  
  const hash = await ospWallet.sendUserOperation({
    to:address.osp,
    value:0,
    data:ospInterface.encodeFunctionData("createActivity",[{
      profileId,
      communityId,
      contentURI:'hdbejhfvehfv',
      extension:address.openLink,
      extensionInitData:ethers.utils.defaultAbiCoder.encode(['uint256'],[ethers.utils.parseEther('0.001')])
    }])
  });
  console.log(hash);
  const receipt = await ospWallet.getUserOperationReceipt(hash);
  const log =receipt.receipt.logs.find(log=>{
    return log.topics[0]==ospInterface.getEventTopic("ActivityCreated");
  });
  const event = ospInterface.decodeEventLog("ActivityCreated",log?.data as string,log?.topics);
  const contentId=event.contentId;
  console.log(`contentId:${contentId}`);

  const openLink=TradeOpenlink__factory.connect(address.openLink,ethers.provider.getSigner());
  console.log(`orderId:${await openLink.getOrderId(profileId,contentId)}`);
  console.log('order:');
  console.log(await openLink.getOrder(profileId,contentId));
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
