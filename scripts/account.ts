/* eslint-disable @typescript-eslint/no-unused-vars */
import { ethers } from "hardhat";
import { BundlerJsonRpcProvider, GasEstimatorMiddleware, GasPriceMiddleware, OspWallet } from "xzx-test-osp-wallet";

const accountFactoryAddress = "0x000000d2593e5589f0c50A2572f4C616f53674fa";
const url =
  "https://eth-sepolia.g.alchemy.com/v2/NPGcgFpos0hKSX_4MGQ_pw-60HHBHzd2";
const bundlerRpcUrl =
  "https://api.stackup.sh/v1/node/cab58bbcf87742c35425ab147fff3e852f3128330e87f88cd4676580a775ec4b";

const provider = new BundlerJsonRpcProvider(url, bundlerRpcUrl);
export const ospWallet = new OspWallet(provider, ethers.provider.getSigner(), accountFactoryAddress);
ospWallet.sender.addMiddleware(GasPriceMiddleware(provider), 0);
ospWallet.sender.addMiddleware(GasEstimatorMiddleware(provider), 1);