/* eslint-disable @typescript-eslint/no-unused-vars */
import hre from "hardhat";
import { OspClient__factory } from "osp-blockchain-types";
import address from "./address.json";

async function main() {
  const osp = OspClient__factory.connect(address.osp, hre.ethers.provider.getSigner());
  await (await osp.whitelistApp(address.openLink, true)).wait()
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
