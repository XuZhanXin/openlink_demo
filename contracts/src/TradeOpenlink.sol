// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import {ActivityExtensionBase} from "@opensocial/contracts/core/base/ActivityExtensionBase.sol";
import {IStandardExecutor} from "@abstractaccount/contracts/interfaces/IStandardExecutor.sol";
import {MockTrade} from "./MockTrade.sol";

contract TradeOpenlink is ActivityExtensionBase {
    MockTrade public immutable trade;

    mapping(uint256 => mapping(uint256 => uint256)) private orderIds;

    constructor(address osp, MockTrade _trade) ActivityExtensionBase(osp) {
        trade = _trade;
    }

    function _initializeActivityExtension(uint256 profileId, uint256 contentId, bytes calldata initData)
        internal
        override
    {
        (uint256 value) = abi.decode(initData, (uint256));
        address owner = OSP.ownerOf(profileId);
        bytes memory returnValue = IStandardExecutor(owner).execute(
            IStandardExecutor.Execution(address(trade), value, abi.encodeCall(MockTrade.buy, ()))
        );
        uint256 orderId = abi.decode(returnValue, (uint256));
        orderIds[profileId][contentId] = orderId;
    }

    function getOrderId(uint256 profileId, uint256 contentId) public view returns (uint256) {
        return orderIds[profileId][contentId];
    }

    function getOrder(uint256 profileId, uint256 contentId) public view returns (MockTrade.Order memory) {
        uint256 orderId = getOrderId(profileId, contentId);
        return trade.getOrderById(orderId);
    }
}
