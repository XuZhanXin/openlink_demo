// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.20;

import {ERC20} from "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract MockTrade is ERC20 {
    uint256 private orderCount;

    struct Order {
        address sender;
        uint256 amount;
    }

    mapping(uint256 => Order) private orders;

    constructor() ERC20("TRADE_ERC20", "TRADE_ERC20") {}

    function buy() external payable returns (uint256 orderId) {
        orderId = ++orderCount;
        orders[orderId] = Order(msg.sender, msg.value);
        _mint(msg.sender, msg.value);
    }

    function getOrderById(uint256 id) public view returns (Order memory) {
        return orders[id];
    }
}
